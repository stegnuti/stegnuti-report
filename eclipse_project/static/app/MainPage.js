Vue.component("chooseLog", {
    
    data: function() {
        return {
            projectList: []
        }
    },

    template: `
    <body class="text-center">
        <form class="form-chooselog">
            <h1 class="h2 mb-3 font-weight-normal"><i>stegnuti</i>|<b>report</b></h1>
            <br>
            <select class="selectpicker" id="projectSelect" data-live-search="true" data-title="Choose a project" data-width="300px" data-style="btn-dark">
                <option v-for="project in projectList">
                    {{project}}
                </option>
            </select>
            <br><br>
            <h3>OR</h3>
            <br>
            <input type="file" accept=".stgr" ref="fileField" class="form-control" id="fileField" @change="uploadFile()">
            <h5 id="loadingMsg" style="display: none; color: gray;">Uploading and processing...</h5>
            <p class="mt-5 mb-3 text-muted">© 2020-2021</p>
        </form>
    </body>
    `
    ,

    methods: {
        uploadFile : function(event) {
            var file = this.$refs.fileField.files[0];
            
            // Hide file field and show loading
            $("#fileField").hide();
            $("#loadingMsg").show();

            // Upload file
            axios
            .post("rest/postLog/" + file.name, file)
            .then(response => {
                this.$router.push("/viewReport/" + response.data.game + "/" + response.data.log);
            })
            .catch(function(error) { alert(error); });
        }
    },

    mounted() {
        var self = this;
        // Pull project list
        axios
        .get("rest/projects")
        .then(response => {
            this.projectList = response.data;
        })
        .catch(function(error) { alert(error); });

        // Load game page event handler
        $('#projectSelect').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            var projectName = e.target.value;
            self.$router.push("/projectView/" + projectName);
          });          
    }

});