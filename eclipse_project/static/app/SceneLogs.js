Vue.component("sceneLogs", {
    
    props: ['lines', 'sceneName'],

    data: function() {
        return {
            myLines: this.lines
        }
    },


    template: `
    <div>
        <div class="list-group list-group-flush">
            <h4>{{sceneName}}</h4>
            <div v-for="(line, index) in myLines" @click="lineClick(index)">
                <router-link to="#" class="list-group-item list-group-item-action" :class="{ 'list-group-item-warning' : line.type === 2, 'list-group-item-danger' : line.type === 0 || line.type === 4, 'list-group-item-secondary' : line.type !== 0 && line.type !== 4 }">
                    <span style="color: gray; font-weight: 200;">
                        [{{ line.timestamp }}] 
                    </span> 
                    <span style="font-weight: 600">
                        {{ line.message }}
                    </span>

                    <span v-if="line.showDetails">
                        <pre>{{ line.trace }}</pre>
                        <img v-if="line.imgName !== ''" :src="line.imgName" height="500">
                    </span>
                </router-link>
            </div>
        </div>
    </div>
    `
    ,

    methods: {
        lineClick: function(lineIndex) {
            this.myLines[lineIndex].showDetails = !this.myLines[lineIndex].showDetails;
            Vue.set(this.myLines, lineIndex, this.myLines[lineIndex]);
            //alert("click to " + line.showDetails);
        }
    }

});